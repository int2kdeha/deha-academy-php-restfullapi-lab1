<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    // protected $model = Product::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = 'sir_' . $this->faker->name();
        $slug = str_replace(" ", "-", $name);
        $user = User::first();
        return [
            'name' => $name,
            'slug' => $slug,
            'description' => $this->faker->realText(20),
            'price' => $this->faker->randomFloat(2, 0, 1000),
            'user_id' => $user->id
        ];
    }
}
