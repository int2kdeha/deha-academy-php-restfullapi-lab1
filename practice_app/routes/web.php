<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * This single route declaration creates multiple routes to handle a variety of actions on the resource.
 * The generated controller will already have methods stubbed for each of these actions.
 * Remember, you can always get a quick overview of your application's routes by running the route:list Artisan command.
 */

Route::get('/displayPosts', function() {
    $postList = Post::all();
    return view('displayPosts', [
        'postList' => $postList
    ]);
} );

Route::get('/', function() {
    return view('welcome');
});

Route::get('/layout', function() {
    return view('layout');
})->name('layout');
