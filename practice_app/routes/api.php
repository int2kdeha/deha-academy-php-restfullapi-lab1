<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\NoPatchMiddleware;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\API\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('posts', PostController::class)->middleware([NoPatchMiddleware::class]);
Route::get('users', [UserController::class, 'index']);

// Public routes
Route::post('/register', [AuthController::class, 'register'])->name('users.register');
Route::post('/login', [AuthController::class, 'login'])->name('users.login');
Route::get('/products/search/{name}', [ProductController::class, 'search'])->name('products.search');
Route::get('/products',  [ProductController::class, 'index'])->name('products.index');
Route::get('/products/{id}', [ProductController::class, 'show'])->name('products.show');

// Protected
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/products', [ProductController::class, 'store'])->name('products.store');
    Route::put('/products/{id}', [ProductController::class, 'update'])->name('products.update');
    Route::delete('/products/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
    Route::post('/logout', [AuthController::class, 'logout'])->name('users.logout');
});
