<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Eloquent
{
    use HasFactory;
    protected $table = 'posts';
    protected $fillable = [
        'name',
        'body'
    ];
}
