<?php

namespace App\Models;

// use Database\Factories\ProductFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Product
 *
 * @mixin Builder
 */

class Product extends Eloquent
{
    use HasFactory;
    
    // /** @return ProductFactory */
    // protected static function newFactory()
    // {
    //     return ProductFactory::new();
    // }

    /**
     * Get the user that owns the PostController
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }

    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'price',
        'user_id'
    ];
}
