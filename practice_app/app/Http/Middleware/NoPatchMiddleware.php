<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NoPatchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->method() === 'PATCH') {
            return response()->json([
                'status' => Response::HTTP_METHOD_NOT_ALLOWED,
                'message' => 'Patch method not allowed for this resource'
            ], Response::HTTP_METHOD_NOT_ALLOWED);
        }
        return $next($request);
    }
}
