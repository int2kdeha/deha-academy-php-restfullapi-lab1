<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sentSuccessResponse($data = '', $message = 'SUCCESS', $status = 200)
    {
        return response([
            'data' => $data,
            'message' => $message
        ], $status);
    }

    public function createSuccessResponse($data = '', $message = 'SUCCESS', $status = 202, $localtion = '')
    {
        return response([
            'data' => $data,
            'message' => $message,
            'location' => $localtion
        ], $status);
    }
}
