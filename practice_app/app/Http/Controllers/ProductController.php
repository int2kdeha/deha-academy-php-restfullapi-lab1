<?php

namespace App\Http\Controllers;

use App\Models\Product;
use \Illuminate\Http\Response;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    protected $product;

    /**
     * @param product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tmp = Product::paginate(10);
        $productsCollection = (new ProductCollection($tmp))->response()->getData(true);
        return $this->sentSuccessResponse($productsCollection, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $newProduct = Product::create($request->all());
        $prunedNewProduct = new ProductResource($newProduct);
        return response()->json([
            'data' => $prunedNewProduct
        ], response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrfail($id);
        $productResource = new ProductResource($product);
        return Response()->json(['data' => $productResource], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::findOrfail($id);
        $product->update($request->all());
        $productResource = new ProductResource($product);
        return Response()->json(['data' => $productResource], Response::HTTP_OK);
    }

    /**
     * Search for a name
     *
     * @param  str  $name
     * @return \Illuminate\Http\Response
     */
    public function search($name)
    {
        $products = Product::where('name', 'like', '%' . $name . '%')
            ->paginate(10);
        $productsCollection = (new ProductCollection($products))->response()->getData(true);
        return $this->sentSuccessResponse($productsCollection, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        $productResource = new productResource($product);
        return response()->json([
            'data' => $productResource
        ], Response::HTTP_OK);
    }
}
