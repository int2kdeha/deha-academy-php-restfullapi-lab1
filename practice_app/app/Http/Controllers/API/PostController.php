<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

class PostController extends Controller
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function scopeFunny($query)
    {
        // usage: Post::scopeFunny()->get()
        return $query->where('name', '%funny%');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginatedPost = Post::paginate(10);
        $postCollection = (new PostCollection($paginatedPost))->response()->getData(true);
        return $this->sentSuccessResponse($postCollection, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $newPost = Post::create($request->all());
        $responsePost = (new PostResource($newPost));
        return response()->json([
            'data' => $responsePost
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $responsePost = $post;
        return $this->sentSuccessResponse($responsePost, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        $statusUpdate = $post->update($request->all());
        $responsePost = (new PostResource($post));
        return response()->json([
            'data' => $responsePost,
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        $postResource = new PostResource($post);
        return response()->json([
            'data' => $postResource
        ], Response::HTTP_OK);
    }
}
