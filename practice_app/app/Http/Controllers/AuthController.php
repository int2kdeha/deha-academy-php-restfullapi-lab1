<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response; // custom response
use Illuminate\Support\Facades\Hash; // custom hasher for password

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);
        /** @var User $user */
        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password'])
        ]);
        /**
         * The field under validation must have a matching field of {field}_confirmation.
         * For example, if the field under validation is password,
         * a matching password_confirmation field must be present in the input.
         */

        $token = $user->createToken('myAppToken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json([
            'data' => $response
        ], Response::HTTP_CREATED);
    }

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // check email
        $user = User::where('email', '=', $fields['email'])->first();

        // check password
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response(["message" => "bad creds"], 401);
        }

        /**
         * The field under validation must have a matching field of {field}_confirmation.
         * For example, if the field under validation is password,
         * a matching password_confirmation field must be present in the input.
         */

        $token = $user->createToken('myAppToken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json([
            'data' => $response
        ], Response::HTTP_CREATED);
    }

    public function logout(User $user)
    {
        $user->tokens()->delete();
        return [
            'message' => 'Logged out'
        ];
    }
}
