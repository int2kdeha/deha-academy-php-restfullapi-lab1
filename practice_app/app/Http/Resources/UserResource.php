<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $products = $this->products;
        foreach ($products as $index => $productInfo) {
            $product = (new ProductResource($productInfo))->formatArrayElement();
            unset($product['user']);
            unset($product['user_id_verbose']);
            $products[$index] = $product;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'products' => $products
        ];
    }
}
