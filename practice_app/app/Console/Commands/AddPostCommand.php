<?php

namespace App\Console\Commands;

use App\Models\Post;
use Illuminate\Console\Command;

class AddPostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:make-post {name?} {body?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fake make a post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = \Faker\Factory::create();
        $name = $faker->name();
        $body = $faker->text();
        $refusal = 0;
        if (!$this->argument('name')) {
            $tmp = $this->ask('What\'s post name?');
            if ($tmp) {
                $name = $tmp;
            } else {
                $refusal += 1;
            }
        }
        if (!$this->argument('body')) {
            $tmp = $this->ask('What\'s post body?');
            if ($tmp) {
                $body = $tmp;
            } else {
                $refusal += 1;
            }
        }
        if ($refusal === 2) {
            $this->warn("no post was added");
            return 0;
        }
        $post = Post::create([
            'name' =>  $name,
            'body' => $body
        ]);
        $this->info('name: ' . $name);
        $this->info('body: ' . $body);
        return 0;
    }
}
