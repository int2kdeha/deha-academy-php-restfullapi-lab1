<?php

namespace Tests\Feature\Posts;

use Tests\TestCase;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function user_can_update_post_if_post_exists_and_data_is_valid()
    {
        $faker = \Faker\Factory::create();
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => $faker->name(),
            'body' => $faker->text()
        ];
        $response = $this->put(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('name', $dataUpdate['name'])
                ->etc()
            )
            ->etc()
        );
        $this->assertDatabaseHas('posts', [
            'name' => $dataUpdate['name'],
            'body' => $dataUpdate['body']
        ]);
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_name_is_null()
    {
        $faker = \Faker\Factory::create();
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => $faker->text()
        ];
        $response = $this->put(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('name')
                ->etc()
            )
            ->etc()
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_body_is_null()
    {
        $faker = \Faker\Factory::create();
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => $faker->name(),
            'body' => ''
        ];
        $response = $this->put(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('body')
                ->etc()
            )
            ->etc()
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_data_is_not_valid()
    {
        $faker = \Faker\Factory::create();
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => ''
        ];
        $response = $this->put(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('body')
                ->has('name')
            )
            ->etc()
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_not_exist_and_data_is_valid()
    {
        $faker = \Faker\Factory::create();
        $postId = -1;
        $dataUpdate = [
            'name' => $faker->name(),
            'body' => $faker->text()
        ];
        $response = $this->put(route('posts.update', $postId), $dataUpdate, [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has('status')
            ->has('message')
        );
    }
}
