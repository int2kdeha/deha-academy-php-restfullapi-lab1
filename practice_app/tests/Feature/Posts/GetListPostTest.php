<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Testing\Fluent\AssertableJson;
use Log;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_posts()
    {
        $postCount = Post::count();
        // Log::info($postCount);
        $response = $this->get(route('posts.index'));
        $response->assertStatus(200);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->has('data')->has(
                    'meta',
                    fn (AssertableJson $json) =>
                    $json->where('total', $postCount)->etc()
                )->has('links')
            )->has('message')
        );
    }
}
