<?php

namespace Tests\Feature\Posts;

use Faker\Factory;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class CreatePostTest extends TestCase
{
    /** @test */
    public function user_can_create_post_if_data_is_valid()
    {
        $faker = Factory::create();
        $dataCreate = [
            'name' => $faker->name(),
            'body' => $faker->text()
        ];
        $response = $this->post(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('name', $dataCreate['name'])
                ->etc()
            )
            ->etc()
        );

        $this->assertDatabaseHas('posts', [
            'name'=>$dataCreate['name'],
            'body'=>$dataCreate['body']
        ]);
    }

    /** @test */
    public function user_can_not_create_post_if_name_is_null()
    {
        $faker = Factory::create();
        $dataCreate = [
            'name' => null,
            'body' => $faker->text()
        ];
        $response = $this->post(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('name')
                ->etc()
            )
            ->etc()
        );
    }

    /** @test */
    public function user_can_not_create_post_if_body_is_null()
    {
        $faker = Factory::create();
        $dataCreate = [
            'name' => $faker->name(),
            'body' => ''
        ];
        $response = $this->post(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('body')
                ->etc()
            )
            ->etc()
        );
    }

    /** @test */
    public function user_can_not_create_post_if_data_is_not_valid()
    {
        $faker = Factory::create();
        $dataCreate = [
            'name' => null,
            'body' => null
        ];
        $response = $this->post(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'errors',
                fn (AssertableJson $json) => $json
                ->has('body')
                ->has('name')
            )
            ->etc()
        );
    }
}
