<?php

namespace Tests\Feature\Posts;

use Tests\TestCase;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_post_if_post_exists()
    {
        $faker = \Faker\Factory::create();
        $post = Post::factory()->create();
        $numberOfPostsBeforeDeletion = Post::count();
        $dataUpdate = [
            'name' => $faker->name(),
            'body' => $faker->text()
        ];
        $response = $this->json('DELETE', route('posts.destroy', $post->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('name', $post->name)
                ->etc()
            )
            ->etc()
        );

        $numberOfPostsAfterDeletion = Post::count();
        $this->assertEquals($numberOfPostsAfterDeletion + 1, $numberOfPostsBeforeDeletion);
    }

    /** @test */
    public function user_can_not_delete_post_if_post_not_exists()
    {
        $faker = \Faker\Factory::create();
        $postId = -1;
        $numberOfPostsBeforeDeletion = Post::count();
        $dataUpdate = [
            'name' => $faker->name(),
            'body' => $faker->text()
        ];
        $response = $this->json('DELETE', route('posts.destroy', $postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

        $numberOfPostsAfterDeletion = Post::count();
        $this->assertEquals($numberOfPostsAfterDeletion, $numberOfPostsBeforeDeletion);
    }
}
