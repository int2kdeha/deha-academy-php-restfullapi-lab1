<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class GetProductTest extends TestCase
{
    /** @test */
    public function user_can_get_product_if_product_exists()
    {
        $product = Product::factory()->create();
        $response = $this->getJson(route('products.show', $product->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('name', $product->name)
                ->etc()
            )
        );
    }
}
