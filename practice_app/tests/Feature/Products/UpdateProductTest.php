<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function user_can_update_product_when_authorize()
    {
        /** @var Authenticatable $user */
        $user =  (User::factory()->create());
        $product = (Product::factory()->create());
        $this->actingAs($user);
        $faker = \Faker\Factory::create();
        $data = [
            'price' => $faker->numberBetween(0, 1000)
        ];
        $response = $this->put(
            route('products.update', $product->id),
            $data,
            ['Accept'=>'application/json']
        );
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('price', $data['price'])
                ->etc()
            )
        );
    }

    /** @test */
    public function user_can_not_update_product_when_not_authorize()
    {
        /** @var Authenticatable $user */
        $user =  (User::factory()->create());
        $product = (Product::factory()->create());
        $faker = \Faker\Factory::create();
        $data = [
            'price' => $faker->numberBetween(0, 1000)
        ];
        $response = $this->put(
            route('products.update', $product->id),
            $data,
            ['Accept'=>'application/json', 'Authorization'=>'Bearer 1|Bullsh1tTo0kee1n']
        );
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
