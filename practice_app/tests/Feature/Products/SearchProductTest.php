<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class SearchProductTest extends TestCase
{
    /** @test */
    public function user_search_product_with_name()
    {
        $nameToSearch = 'ma';
        $response = $this->get(
            route('products.search', $nameToSearch),
            ['accept' => 'application/json']
        );
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has('data')
                ->has('message')
        );
    }
}
