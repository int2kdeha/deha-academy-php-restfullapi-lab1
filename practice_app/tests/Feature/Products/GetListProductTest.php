<?php

namespace Tests\Feature\Products;

use Tests\TestCase;

class GetListProductsTest extends TestCase
{
    /** @test */
    public function can_list_products()
    {
        $response = $this->get(route('products.index'));
        $response->assertStatus(200);
    }
}
