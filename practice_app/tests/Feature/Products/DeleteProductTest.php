<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function user_can_delete_product_if_product_exists()
    {
        $faker = \Faker\Factory::create();
        $product = Product::factory()->create();
        /** @var User */
        $user = User::factory()->create();
        $this->actingAs($user);
        $numberOfProductsBeforeDeletion = Product::count();
        $response = $this->json('DELETE', route('products.destroy', $product->id), [], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->where('name', $product->name)
                ->etc()
            )
            ->etc()
        );

        $numberOfProductsAfterDeletion = Product::count();
        $this->assertEquals($numberOfProductsAfterDeletion + 1, $numberOfProductsBeforeDeletion);
    }

    /** @test */
    public function user_can_not_delete_product_if_product_not_exists()
    {
        $faker = \Faker\Factory::create();
        $productId = -1;
        /** @var User */
        $user = User::factory()->create();
        $this->actingAs($user);
        $numberOfProductsBeforeDeletion = Product::count();
        $response = $this->json('DELETE', route('products.destroy', $productId), [], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has('message')
            ->etc()
        );
        $numberOfProductsAfterDeletion = Product::count();
        $this->assertEquals($numberOfProductsAfterDeletion, $numberOfProductsBeforeDeletion);
    }
}
