<?php

namespace Tests\Feature\Products;

use App\Models\User;
use Illuminate\Http\Response;
use Log;
use Tests\TestCase;
use Illuminate\Contracts\Auth\Authenticatable; // <- this part is added

class CreateProductTest extends TestCase
{
    /** @test */
    public function user_can_not_create_product_when_not_authorize()
    {
        // $this->withoutExceptionHandling(); // forget exception error like 500
        $faker = \Faker\Factory::create();
        $name = 'sir_' . $faker->firstNameFemale();
        $data = [
            'name' => $name,
            'slug' => str_replace(' ', '-', $name),
            'description' => $faker->text(),
            'price' => $faker->numberBetween(1, 1000)
        ];
        // Log::info($data);
        //$this->actingAs(user);
        $response = $this->post(
            route('products.store'),
            $data,
            ['Accept' => 'application/json', 'Authorization' => 'Bearer 1|Bullsh1tTo0kee1n']
        );
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_can_create_product_when_authorize()
    {
        /** @var Authenticatable $user */
        $user =  (User::factory()->create());
        $this->actingAs($user);
        $faker = \Faker\Factory::create();
        $name = 'madam_' . $faker->firstNameFemale();
        $data = [
            'name' => $name,
            'slug' => str_replace(' ', '-', $name),
            'description' => $faker->text(),
            'price' => $faker->numberBetween(0, 1000)
        ];
        $response = $this->post(
            route('products.store'),
            $data,
            ['Accept' => 'application/json']
        );
        // $content = $response->getContent();
        // echo $content;
        $response->assertStatus(Response::HTTP_OK);
    }
}
