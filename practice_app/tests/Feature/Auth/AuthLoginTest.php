<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class AuthLoginTest extends TestCase
{
    /** @test */
    public function user_can_login_with_correct_mail_and_password()
    {
        $faker = \Faker\Factory::create();

        // register an user
        $userPassword =  $faker->text();
        $userEmail = $faker->unique()->email();
        $userInfo = [
            'name' => $faker->name(),
            'email' => $userEmail,
            'password' => $userPassword,
            'password_confirmation' => $userPassword
        ];
        $response = $this->post(route('users.register'), $userInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->has('user')
                        ->has('token')
                )
        );

        //make user login
        $userLoginInfo = [
            'email' => $userEmail,
            'password' => $userPassword,
        ];
        $response = $this->post(route('users.login'), $userLoginInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        // dd($response);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->has(
                            'user',
                            fn (AssertableJson $json) => $json
                                ->where('email', $userLoginInfo['email'])
                                ->etc()
                        )
                        ->has('token')
                )
        );
    }

    /** @test */
    public function user_can_not_login_with_incorrect_password()
    {
        $faker = \Faker\Factory::create();

        // register an user
        $userPassword =  $faker->text();
        $userEmail = $faker->unique()->email();
        $userInfo = [
            'name' => $faker->name(),
            'email' => $userEmail,
            'password' => $userPassword,
            'password_confirmation' => $userPassword
        ];
        $response = $this->post(route('users.register'), $userInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->has('user')
                        ->has('token')
                )
        );

        //make user login
        $userLoginInfo = [
            'email' => $userEmail,
            'password' => "BULL SHIT PASSWORD",
        ];
        $response = $this->post(route('users.login'), $userLoginInfo);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        // dd($response);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has('message')
        );
    }

    /** @test */
    public function user_can_not_login_with_incorrect_mail()
    {
        $faker = \Faker\Factory::create();

        // register an user
        $userPassword =  $faker->text();
        $userEmail = $faker->unique()->email();
        $userInfo = [
            'name' => $faker->name(),
            'email' => $userEmail,
            'password' => $userPassword,
            'password_confirmation' => $userPassword
        ];
        $response = $this->post(route('users.register'), $userInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->has('user')
                        ->has('token')
                )
        );

        //make user login
        $userLoginInfo = [
            'email' => "sweet creature, sweet creature, wherever I go, you bring me home",
            'password' => $userPassword,
        ];
        $response = $this->post(route('users.login'), $userLoginInfo);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        // dd($response);
        $response->assertJson(
            fn (AssertableJson $json) => $json
                ->has('message')
        );
    }
}
