<?php

namespace Tests\Feature\Users;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    /** @test */
    public function user_can_register_with_mail_and_password()
    {
        $faker = \Faker\Factory::create();
        $userPassword =  $faker->text();
        $userInfo = [
            'name' => $faker->name(),
            'email' => $faker->unique()->email(),
            'password' => $userPassword,
            'password_confirmation' => $userPassword
        ];
        $response = $this->post(route('users.register'), $userInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(
            fn (AssertableJson $json) => $json
            ->has(
                'data',
                fn (AssertableJson $json) => $json
                ->has('user')
                ->has('token')
            )
        );
    }

    /** @test */
    public function user_can_not_register_with_existing_mail()
    {
        $faker = \Faker\Factory::create();
        $userPassword1 =  $faker->text();
        $sameEmail = $faker->unique()->email();
        $userInfo1 = [
            'name' => $faker->name(),
            'email' => $sameEmail,
            'password' => $userPassword1,
            'password_confirmation' => $userPassword1
        ];
        $response1 = $this->json('POST', route('users.register'), $userInfo1);
        $userPassword2=  $faker->text();
        $userInfo2 = [
            'name' => $faker->name(),
            'email' => $sameEmail,
            'password' => $userPassword2,
            'password_confirmation' => $userPassword2
        ];
        $response2 = $this->post(route('users.register'), $userInfo2, ['Accept'=>'application/json']);
        $response2->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
