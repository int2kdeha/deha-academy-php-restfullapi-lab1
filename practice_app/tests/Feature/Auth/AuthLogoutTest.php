<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Http\Response;

class AuthLogoutTest extends TestCase
{
    /** @test */
    public function user_can_logout_with_correct_token()
    {
        $faker = \Faker\Factory::create();
        $userPassword =  $faker->text();
        $userEmail = $faker->unique()->email();
        $userInfo = [
            'name' => $faker->name(),
            'email' => $userEmail,
            'password' => $userPassword,
            'password_confirmation' => $userPassword
        ];
        $userLoginInfo = [
            'email' => $userEmail,
            'password' => $userPassword,
        ];
        $response = $this->post(route('users.register'), $userInfo);
        $response->assertStatus(Response::HTTP_CREATED);
        $response = $this->post(route('users.login'), [
            'email' => $userLoginInfo['email'],
            'password' => $userLoginInfo['password']
        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(Response::HTTP_CREATED);
        echo($response['data']['token']);
        $response = $this->post(route('users.logout'), [], [
            'Accept' => 'application/json', 'Authorization' =>  'Bearer ' . $response['data']['token']
        ]);
        $response->assertStatus(Response::HTTP_OK);
    }
}
